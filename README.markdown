
Follow these steps to get started:

1. Git-clone this repository.

        $ git clone https://tomekl007@bitbucket.org/tomekl007/packt-akka-http.git

2. Change directory into your clone:

        $ cd my-project

3. Launch SBT:

        $ sbt

4. Compile everything and run all tests:

        > test

5. Start the application:

        > re-start

6. Browse to [http://localhost:8080](http://localhost:8080/)

7. Stop the application:

        > re-stop

8. Learn more at http://www.spray.io/
