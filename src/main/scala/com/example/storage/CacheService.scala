package com.example.storage

import spray.caching.Cache

import scala.concurrent.Future
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.ExecutionContext.Implicits.global


trait StorageService[V] {
  def get(key: String): scala.Option[scala.concurrent.Future[V]]

  def put(key: String, value: String)
}

class InMemoryStorage(cache: Cache[String]) extends StorageService[String] {
  override def get(key: String): Option[Future[String]] = cache.get(key)

  override def put(key: String, value: String): Unit = cache(key) {
    value
  }
}
