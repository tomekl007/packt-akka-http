package com.example.storage

import sun.net.www.http.HttpClient

import scala.concurrent.Future


class ExternalDatabaseStorage(httpClient: HttpClient) extends StorageService[String]{
  override def get(key: String): Option[Future[String]] = {
    //send get request to external database
    ???
  }

  override def put(key: String, value: String): Unit = {
    //write data into some external database
   ???
  }
}

