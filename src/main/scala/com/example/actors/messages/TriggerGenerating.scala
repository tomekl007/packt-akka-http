package com.example.actors.messages

case class TriggerGenerating(accountId: String, reportType: String)
