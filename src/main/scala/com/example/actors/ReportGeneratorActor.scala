package com.example.actors

import akka.actor.Actor
import com.example.actors.messages.TriggerGenerating
import com.example.storage.StorageService
import spray.http.DateTime

class ReportGeneratorActor(storageService: StorageService[String]) extends Actor {
  implicit val ec = context.dispatcher

  override def receive: Receive = {
    case TriggerGenerating(accountId, reportType) => {
      //simulate long running action
      Thread.sleep(2000)
      storageService.put(
        accountId + reportType,
        s"reportContent that was generated ${DateTime.now}")
    }
  }
}
