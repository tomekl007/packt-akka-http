package com.example

import akka.actor.{Actor, ActorRef}
import com.example.actors.messages.TriggerGenerating
import com.example.storage.StorageService
import spray.caching.Cache
import spray.http.HttpHeaders.Location
import spray.http.HttpResponse
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.routing._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}


class ReportServiceActor(storageService: StorageService[String], reportGeneratorActor: ActorRef)
  extends Actor with ReportService {

  def actorRefFactory = context
  def receive = runRoute(reportRoute(storageService, reportGeneratorActor))
}

trait ReportService extends HttpService {

  def reportRoute(storageService: StorageService[String],
                  reportGenerateActor: ActorRef): Route =
    (get & pathPrefix("report" / Segment )) {
      case (accountId) =>{
        println("handle")
        parameters("type") { case (reportType) =>
          reportGenerateActor ! TriggerGenerating(accountId, reportType)
          complete {
            new HttpResponse(Accepted, s"get $reportType for $accountId triggered")
              .withHeaders(Location(accountId + reportType))
          }
        }
      }
    } ~ 
      (get & pathPrefix("report" / Segment / Segment)) {
        case(accountId, key) =>{
          getFromStorage(storageService, key)
        }
    }

  def getFromStorage(storageService: StorageService[String], key: String) = {
    respondWithMediaType(`application/json`) {
      storageService.get(key).map { x =>
        val rez = Await.result(x, Duration.Inf)
        complete(OK, rez)
      }.getOrElse {
        respondWithHeader(Location(key)) {
          complete(Accepted, "{}")
        }
      }
    }
  }
}